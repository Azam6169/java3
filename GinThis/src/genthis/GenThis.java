/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genthis;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ipd
 */
public class GenThis {
    public static void main(String[] args){
        
        
        String DATA ="Jerry White     \n44    Merry ";
        Scanner input = new Scanner(DATA);
        String n1 = input.nextline();
        String n2 = input.next();
        System.out.printf("n1:%s",n1,n2);
                
        //old way
     try{   ArrayList list = new ArrayList();
        //no control over what you put into the list
        list.add("Jerry was here");
        list.add(new Object());
        list.add(new Scanner(System.in));
                
                for (int i=0; i<list.size(); i++){
                    Object o = list.get(i);
                    String s = (String) o ;
                    System.out.println(s);
                }
    }
     catch (ClassCastException e){
    System.out.println("The old way below up; " +e.getMessage());
}
    
    {ArrayList<String> list = new ArrayList<String>();
    list.add("Jerry was here");
    //list.add(new Object());
    for (int i=0; i<list.size(); i++){
                    Object o = list.get(i);
                    String s = (String) o ;
                    System.out.println(s);
                }
    }
    
}
}
