/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel;

import java.util.Date;

/**
 *
 * @author ipd
 */
public class Trip {
    
    private String destination;
    private String travellerName;
    private String travellerPassportNo;
    private Date departureDate;
    private Date ReturnDate;

    /**
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    public Trip(String destination, String travellerName, String travellerPassportNo, Date departureDate, Date ReturnDate) {
        this.destination = destination;
        this.travellerName = travellerName;
        this.travellerPassportNo = travellerPassportNo;
        this.departureDate = departureDate;
        this.ReturnDate = ReturnDate;
    }

    /**
     * @param destination the destination to set
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * @return the travellerName
     */
    public String getTravellerName() {
        return travellerName;
    }

    /**
     * @param travellerName the travellerName to set
     */
    public void setTravellerName(String travellerName) {
        this.travellerName = travellerName;
    }

    /**
     * @return the travellerPassportNo
     */
    public String getTravellerPassportNo() {
        return travellerPassportNo;
    }

    /**
     * @param travellerPassportNo the travellerPassportNo to set
     */
    public void setTravellerPassportNo(String travellerPassportNo) {
        this.travellerPassportNo = travellerPassportNo;
    }

    /**
     * @return the departureDate
     */
    public Date getDepartureDate() {
        return departureDate;
    }

    /**
     * @param departureDate the departureDate to set
     */
    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    /**
     * @return the ReturnDate
     */
    public Date getReturnDate() {
        return ReturnDate;
    }

    /**
     * @param ReturnDate the ReturnDate to set
     */
    public void setReturnDate(Date ReturnDate) {
        this.ReturnDate = ReturnDate;
    }
    @Override
    public String toString() {
        return (getTravellerName() + "(" + getTravellerPassportNo() + ") to " + getDestination() + " on " + getDepartureDate());
    }

}
